import React from 'react'
import { connect } from 'react-redux'
import { setFetchedMessages, editMessage } from '../../actions'
import './Chat.css'

import ChatBox from './../../components/chatBox/ChatBox'
import SiteHeader from './../../components/SiteHeader/SiteHeader'
import MessageInput from './../../components/MessageInput/MessageInput'
import Footer from './../../components/Footer/Footer'
import Modal from './../../components/Modal/Modal'
import Spinner from './../../components/Spinner/Spinner'
import MessageModel from '../../models/MessageModel'

class Chat extends React.Component {
  componentDidMount() {
    fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
      .then((res) => res.json())
      .then(
        (result) => {
          const chatItems = result
            .map((item) => new MessageModel(item))
            .sort((a, b) => a.createdAt - b.createdAt)
          this.props.setFetchedMessages(chatItems)
        },
        (error) => {
          console.log('smth went wrong')
        }
      )

    window.addEventListener('keydown', (e) => {
      if (e.keyCode == 38) {
        const myMessages = this.props.chatItems.filter((item) => item.isMine)
        if (myMessages.length) {
          const myLastMessage = myMessages[myMessages.length - 1]
          this.props.editMessage(myLastMessage.id, myLastMessage.text)
        }
      }
    })
  }

  render() {
    return (
      <div className="content-container">
        {this.props.showSpinner ? <Spinner /> : ''}
        <SiteHeader></SiteHeader>
        <ChatBox />
        <MessageInput />
        <Footer></Footer>
        {this.props.currentlyEditedMessage && <Modal />}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  chatItems: state.chatItems,
  currentlyEditedMessage: state.currentlyEditedMessage,
  showSpinner: state.showSpinner,
})

const mapDispatchToProps = {
  setFetchedMessages,
  editMessage,
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
