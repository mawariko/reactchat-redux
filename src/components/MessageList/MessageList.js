import React, { useEffect, useRef } from 'react'
import { connect } from 'react-redux'
import Message from '../Message/Message'
import MessageDivider from '../MessageDivider/MessageDivider'

function MessageList({ chatItems }) {
  const messagesEndRef = useRef(null)

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: 'auto' })
  }

  const items = []
  let lastDate = null
  chatItems.forEach((chatItem) => {
    const date = chatItem.createdAt.format('DD MMMM')
    if (date !== lastDate) {
      lastDate = date
      items.push(<MessageDivider date={chatItem.createdAt} key={lastDate} />)
    }

    items.push(
      <Message {...chatItem} key={chatItem.id} ></Message>
    )
  })

  useEffect(scrollToBottom, [items])
  return (
    <div>
      {items} <div ref={messagesEndRef} />
    </div>
  )
}

const mapStateToProps = (state) => ({
  chatItems: state.chatItems,
})

export default connect(mapStateToProps)(MessageList)
