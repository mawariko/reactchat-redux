import {
  SET_FETCHED_MESSAGES,
  TOGGLE_LIKE,
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  UPDATE_MESSAGE,
  CANCEL_EDIT_MESSAGE,
} from './actionTypes'

export const setFetchedMessages = (messages) => ({
  type: SET_FETCHED_MESSAGES,
  messages,
})

export const toggleLike = (id) => ({
  type: TOGGLE_LIKE,
  id,
})

export const createMessage = (text) => ({
  type: CREATE_MESSAGE,
  text,
})

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  id,
})

export const editMessage = (id, text) => ({
  type: EDIT_MESSAGE,
  payload: { id, text },
})

export const updateMessage = (text) => ({
  type: UPDATE_MESSAGE,
  text,
})

export const cancelEditMessage = () => ({
  type: CANCEL_EDIT_MESSAGE,
})
