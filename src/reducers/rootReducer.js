import {
  SET_FETCHED_MESSAGES,
  TOGGLE_LIKE,
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  UPDATE_MESSAGE,
  CANCEL_EDIT_MESSAGE,
} from './../actions/actionTypes'
import MessageModel from '../models/MessageModel'
import moment from 'moment'

const initialState = {
  chatItems: [],
  showSpinner: true,
  currentlyEditedMessage: null,
}

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case SET_FETCHED_MESSAGES:
      return {
        ...state,
        chatItems: action.messages,
        showSpinner: false,
      }
    case TOGGLE_LIKE:
      const likedChatItems = state.chatItems.map((item) => {
        if (item.id === action.id) {
          item.isMyLike = !item.isMyLike
          item.isMyLike ? item.likeCount++ : item.likeCount--
        }
        return item
      })
      return {
        ...state,
        chatItems: likedChatItems,
      }
    case CREATE_MESSAGE:
      const message = new MessageModel({
        id: new Date().getTime(),
        avatar: 'https://my-fake-url',
        user: 'Takeshi Kovach',
        userId: 'my-fake-id',
        text: action.text,
        createdAt: new Date(),
        editedAt: null,
        isMine: true,
        likeCount: 0,
        isMylike: false,
      })

      const chatItems = [...state.chatItems]
      chatItems.push(message)

      return {
        ...state,
        chatItems,
      }
    case DELETE_MESSAGE:
      return {
        ...state,
        chatItems: state.chatItems.filter((item) => item.id !== action.id),
      }
    case EDIT_MESSAGE:
      return {
        ...state,
        currentlyEditedMessage: action.payload,
      }
    case UPDATE_MESSAGE:
      if (state.currentlyEditedMessage) {
        const updatedChatItems = state.chatItems.map((item) => {
          if (item.id === state.currentlyEditedMessage.id) {
            item.text = action.text
            item.editedAt = moment()
          }
          return item
        })

        return {
          ...state,
          chatItems: updatedChatItems,
          currentlyEditedMessage: null,
        }
      }
    case CANCEL_EDIT_MESSAGE:
      return {
        ...state,
        currentlyEditedMessage: null,
      }
    default:
      return state
  }
}

export default rootReducer
