import React from 'react';
import ReactDOM from 'react-dom';
import  {createStore} from 'redux';
import rootReducer from './reducers/rootReducer'
import './index.css'
import { Provider } from 'react-redux'
import Chat from './containers/Chat/Chat.js';
import { composeWithDevTools } from 'redux-devtools-extension';


const store = createStore(rootReducer, composeWithDevTools());

ReactDOM.render(
  <Provider store={store}>
  <React.StrictMode>
    <Chat/>
  </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

