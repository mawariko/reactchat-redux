import React from 'react'
import './Spinner.css'
import spinnerGif from './../../images/spinner.gif'

export default function Spinner() {
  return (
    <div className="spinner">
      <img src={spinnerGif} alt="spinner" />
    </div>
  )
}
