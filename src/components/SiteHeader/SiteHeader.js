import React from 'react';
import snail from './../../images/snail.png';
import './SiteHeader.css'

export default function SiteHeader(props) {
    return (
        <div className='site-header'>
         <img src={snail} />
         <h2>Snail Chat</h2>
        </div>
    )
  }