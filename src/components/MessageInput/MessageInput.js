import React, { useState } from 'react'
import { connect } from 'react-redux'
import { createMessage } from '../../actions'
import './MessageInput.css'

function MessageInput({ createMessage }) {
  const [text, setText] = useState('')

  const submitHandler = () => {
    if (text) {
      createMessage(text)
      setText('')
    }
  }

  return (
    <div className="message-input">
      <textarea
        id="message-input"
        name="message-input"
        rows="3"
        cols="110"
        placeholder="Enter your message"
        onChange={(e) => setText(e.target.value)}
        value={text}
      ></textarea>

      <button className="send-button" onClick={submitHandler}>
        SEND
      </button>
    </div>
  )
}

const mapDispatchToProps = {
  createMessage,
}

export default connect(null, mapDispatchToProps)(MessageInput)
